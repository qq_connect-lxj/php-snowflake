<div>
  <p align="center">
    <image src="https://www.pngkey.com/png/full/105-1052235_snowflake-png-transparent-background-snowflake-with-clear-background.png" width="250" height="250">
  </p>
  <p align="center">An ID Generator for PHP based on Snowflake Algorithm (Twitter announced).</p>
  <p align="center">
    <a href="https://github.com/godruoyi/php-snowflake/actions/workflows/php.yml">
      <image src="https://github.com/godruoyi/php-snowflake/actions/workflows/php.yml/badge.svg" alt="build passed">
    </a>
    <a href="https://codecov.io/gh/godruoyi/php-snowflake">
      <img src="https://codecov.io/gh/godruoyi/php-snowflake/branch/master/graph/badge.svg?token=7AAOYCJK97"/>
    </a>
    <a href="https://github.com/godruoyi/php-snowflake">
      <image src="https://poser.pugx.org/godruoyi/php-snowflake/license" alt="License">
    </a>
    <a href="https://packagist.org/packages/godruoyi/php-snowflake">
      <image src="https://poser.pugx.org/godruoyi/php-snowflake/v/stable" alt="Packagist Version">
    </a>
    <a href="https://github.com/godruoyi/php-snowflake">
      <image src="https://poser.pugx.org/godruoyi/php-snowflake/downloads" alt="Total Downloads">
    </a>
  </p>
</div>

## 说明

雪花算法的 PHP 实现

![file](https://images.godruoyi.com/comments/201908/13/_1565668072_AbkRnhQaYk.png)

Snowflake 是 Twitter 内部的一个 ID 生算法，可以通过一些简单的规则保证在大规模分布式情况下生成唯一的 ID 号码。其组成为：

* 第一个 bit 为未使用的符号位。
* 第二部分由 41 位的时间戳（毫秒）构成，他的取值是当前时间相对于某一时间的偏移量。
* 第三部分和第四部分的 5 个 bit 位表示数据中心和机器ID，其能表示的最大值为 2^5 -1 = 31。
* 最后部分由 12 个 bit 组成，其表示每个工作节点**每毫秒**生成的序列号 ID，同一毫秒内最多可生成 2^12 -1 即 4095 个 ID。

需要注意的是：

* 在分布式环境中，5 个 bit 位的 datacenter 和 worker 表示最多能部署 31 个数据中心，每个数据中心最多可部署 31 台节点
* 41 位的二进制长度最多能表示 2^41 -1 毫秒即 69 年，所以雪花算法最多能正常使用 69 年，为了能最大限度的使用该算法，你应该为其指定一个开始时间。

> 由上可知，雪花算法生成的 ID 并不能保证唯一，如当两个不同请求同一时刻进入相同的数据中心的相同节点时，而此时该节点生成的 sequence 又是相同时，就会导致生成的 ID 重复。

所以要想使用雪花算法生成唯一的 ID，就需要保证同一节点同一毫秒内生成的序列号是唯一的。基于此，我们在 SDK 中集成了多种序列号提供者：

* RandomSequenceResolver（随机生成）
* RedisSequenceResolver （基于 redis psetex 和 incrby 生成）
* LaravelSequenceResolver（基于 redis psetex 和 incrby 生成）
* SwooleSequenceResolver（基于 swoole_lock 锁）

不同的提供者只需要保证**同一毫秒生成的序列号不同**，就能得到唯一的 ID。

## 要求

1. PHP >= 7.0
2. **[Composer](https://getcomposer.org/)**

## 安装

```shell
$ composer require liuyajian/php-snowflake -vvv
```

## 使用

1. 简单使用.

```php
$snowflake = new \Liuyajian\Snowflake\Snowflake;

$snowflake->id();
// 1537200202186752
```

2. 指定数据中心ID及机器ID.

```php
$snowflake = new \Liuyajian\Snowflake\Snowflake($datacenterId, $workerId);

$snowflake->id();
```

3. 指定开始时间.

```php
$snowflake = new \Liuyajian\Snowflake\Snowflake;
$snowflake->setStartTimeStamp(strtotime('2019-08-08')*1000);

$snowflake->id();
```

## 高级

1. 在 YII 中使用

因为 SDK 相对简单，我们并没有提供 YII 的扩展包，你可通过下面的方式快速集成到 YII 中。

```<?php

namespace common\service;

use common\components\cache\CacheKey;
use common\components\cache\RedisCache;
use common\consts\CacheConst;
use Exception;
use Godruoyi\Snowflake\RandomSequenceResolver;
use Godruoyi\Snowflake\RedisSequenceResolver;
use Godruoyi\Snowflake\Snowflake;
use Godruoyi\Snowflake\SwooleSequenceResolver;
use Yii;

/**
 * 雪花算法类
 * SnowFlake算法
 * ------------------------------------------------------------------------------------------------------------------------
 * unused                                                         datacenter_id          sequence_id
 * │                     │
 * │                                                              │                     │
 * │                                                              │                     │
 * │  │                                                      │    │                     │
 * │  │                                                      │    │                     │
 * ▼  │◀──────────────────    41 bits   ────────────────────▶│    ▼                     ▼
 * ┌─────┼──────────────────────────────────────────────────────┼────────┬────────┬────────────────┐
 * │  0  │ 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0  │ 00000  │ 00000  │ 0000 0000 0000 │
 * └─────┴──────────────────────────────────────────────────────┴────────┴────────┴────────────────┘
 * ▲                                        ▲
 * │                                        │
 * │                                        │
 * │                                        │
 * │                                        │
 * │                                        │
 * │                                        │
 *
 * time in milliseconds                          worker_id
 * -------------------------------------------------------------------------------------------------------------------------
 * +-----------------------------------------------------------------------------+
 * | 1 Bit Unused | 41 Bit Timestamp |  4 Bit Sequence ID  |   5 Bit datacenter_id |  13 Bit worker_id |
 * 42位时间戳精确到1ms，每1毫秒最多生成 15个ID，datacenter_id【业务标识】 最大 31，worker_id【站点ID】 最大8081
 * +-----------------------------------------------------------------------------+
 * +-----------------------------------------------------------------------------+
 * | 1 Bit Unused | 41 Bit Timestamp |  0 Bit Sequence ID  |   9 Bit datacenter_id |  13 Bit worker_id |
 * 42位时间戳精确到1ms，每1毫秒最多生成 1个ID，datacenter_id【业务标识，可按表分】 最大 511，worker_id【站点ID】 最大8191
 * +-----------------------------------------------------------------------------+
 * 如图所示为snowflake算法使用的一个64 bit的整型数据，被划分为四部分。
 * 不含开头的第一个bit，因为是符号位；
 * 41bit来表示收到请求时的时间戳，精确到1毫秒；
 * 5bit表示数据中心的id， 5bit表示机器实例id
 * 共计10bit的机器位，因此能部署在1024台机器节点上生成ID；
 * 12bit循环自增序列号，增至最大后归0，1毫秒最大生成唯一ID的数量是4096个。
 * 这种机制下，timestamp的41位可以支持我们使用69年。同一台机器下，同一毫秒产生2^12=4096条消息，一秒就是409.6w条消息。且可以部署1024台机器实例。
 * ————————————————
 * @package app\helpers
 */
class SnowFlakeService
{
    const MAX_DATACENTER_LENGTH = 1;

    const MAX_WORKID_LENGTH = 10;//13

    const MAX_SEQUENCE_LENGTH = 11;//8
    /** @var $_snowflake */
    private static $_snowflake;

    /** @var */
    private $_snowflakeClient;

    /**
     * 雪花算法类
     * @param int|null $dataCenter
     * @param int|null $workerId
     * @param string|null $resolver
     * @return
     * @throws Exception
     */
    public static function getInstance($dataCenter = -1, $workerId = -1, $resolver = ''): self
    {
        if (self::$_snowflake === null) {
            self::$_snowflake = new self();
        }
        //@todo 根据目前的测试情况 建议采用：  workid：10bit Sequence：11bit ；院校dataCenterID 指定为1,沃创dataCenterID 指定为0 ；
        $snowflake = new Snowflake(1, -1, self::MAX_DATACENTER_LENGTH, self::MAX_WORKID_LENGTH, self::MAX_SEQUENCE_LENGTH);//指定数据中心ID及机器ID
//        if (!empty($resolver) && $resolver == 'redis'){
        //使用Redis setNx
//        $cachePrefix = 'test:';
//        $cacheKey = $cachePrefix.floor(microtime(true) * 1000);
//        $redis = new \Redis();
//        $redis->connect(\Yii::$app->params['redis']['hostname'], \Yii::$app->params['redis']['port']);
//        $redis->auth(\Yii::$app->params['redis']['password']);
//        $redis->select(14);
//        $redisSequenceResolver = new RedisSequenceResolver($redis);
//        }
        $sequenceResolver = !empty(Yii::$app->params['snowflake']['sequenceResolver']) && Yii::$app->params['snowflake']['sequenceResolver'] == 'random'
            ? new RandomSequenceResolver() : new SwooleSequenceResolver();
        self::$_snowflake->_snowflakeClient = $snowflake
            ->setStartTimeStamp(strtotime('2021-12-27') * 1000)//设置开始时间
            ->setSequenceResolver(
//                $redisSequenceResolver->setCachePrefix($cacheKey)
//                new RandomSequenceResolver()
//                new SwooleSequenceResolver()
                $sequenceResolver
            );
        return self::$_snowflake;
    }

    /**
     * 生成为唯一ID
     * @param bool $isLock
     * @param SnowFlakeRedisService $snowFlakeRedisService
     * @return array
     * @throws Exception
     */
    public function getId($isLock = false, $snowFlakeRedisService = null)
    {
        if ($isLock) {
            //先从缓存取预制ID
            if (!$snowFlakeRedisService){
                $snowFlakeRedisService = Yii::$app->service->snowFlakeRedisService::getInstance();
            }
            $snowId = $snowFlakeRedisService
                ->lpop(CacheKey::SNOW_FLAKE_LOCK_LIST);
            if (empty($snowId)) {
                $snowId = self::$_snowflake->_snowflakeClient->id();
                //分布式id 并发锁
                $cacheKey = CacheKey::SNOW_FLAKE_LOCK;
                //redis  分布式锁
                //加锁 1秒
                $setLock = RedisCache::getInstance(['id' => $snowId])
                    ->setLock($cacheKey, '1', CacheConst::EXPIRED_ONE_SEC);
                if (!$setLock) {
                    $snowId = $this->getId(true);
                }
            }

        } else {
            $snowId = self::$_snowflake->_snowflakeClient->id();
        }
        return $snowId;
    }

    /**
     * Parse snowflake id.
     */
    public static function parseId(string $id, bool $transform = false): array
    {
        $id = decbin($id);

        $data = [
            'timestamp' => substr($id, 0, -(self::MAX_WORKID_LENGTH + self::MAX_DATACENTER_LENGTH + self::MAX_SEQUENCE_LENGTH)),
            'sequence' => substr($id, -self::MAX_SEQUENCE_LENGTH),
            'workerid' => substr($id, -(self::MAX_SEQUENCE_LENGTH + self::MAX_WORKID_LENGTH), self::MAX_WORKID_LENGTH),
            'datacenter' => substr($id, -(self::MAX_SEQUENCE_LENGTH + self::MAX_WORKID_LENGTH + self::MAX_DATACENTER_LENGTH), self::MAX_DATACENTER_LENGTH),
        ];

        return $transform ? array_map(function ($value) {
            return bindec($value);
        }, $data) : $data;
    }

    /**
     * 雪花算法类
     * @param int|null $dataCenter
     * @param int|null $workerId
     * @param string|null $resolver
     * @return
     * @throws Exception
     */
    public static function getInstanceTest($dataCenter = -1, $workerId = -1, $resolver = ''): self
    {
        if (self::$_snowflake === null) {
            self::$_snowflake = new self();
        }
        $snowflake = new Snowflake($dataCenter, $workerId, 1, 10, 11);//指定数据中心ID及机器ID
//        if (!empty($resolver) && $resolver == 'redis'){
        //使用Redis setNx
//        $cachePrefix = 'test:';
//        $cacheKey = $cachePrefix.floor(microtime(true) * 1000);
//        $redis = new \Redis();
//        $redis->connect(\Yii::$app->params['redis']['hostname'], \Yii::$app->params['redis']['port']);
//        $redis->auth(\Yii::$app->params['redis']['password']);
//        $redis->select(14);
//        $redisSequenceResolver = new RedisSequenceResolver($redis);
//        }
        $sequenceResolver = !empty(Yii::$app->params['snowflake']['sequenceResolver']) && Yii::$app->params['snowflake']['sequenceResolver'] == 'random'
            ? new RandomSequenceResolver() : new SwooleSequenceResolver();
        self::$_snowflake->_snowflakeClient = $snowflake
            ->setStartTimeStamp(strtotime('2021-12-27') * 1000)//设置开始时间
            ->setSequenceResolver(
//                $redisSequenceResolver->setCachePrefix($cacheKey)
//                new RandomSequenceResolver()
//                new SwooleSequenceResolver()
                $sequenceResolver
            );
        return self::$_snowflake;
    }
}

```

2. 自定义序列号解决器

你可以通过实现 Godruoyi\Snowflake\SequenceResolver 接口来自定义序列号解决器。

```php
class YourSequence implements SequenceResolver
{
    /**
     *  {@inheritdoc}
     */
    public function sequence(int $currentTime)
    {
          // Just test.
        return mt_rand(0, 1);
    }
}

// usage

$snowflake->setSequenceResolver(new YourSequence);
$snowflake->id();
```

你也可以直接使用闭包：

```php
$snowflake = new \Godruoyi\Snowflake\Snowflake;
$snowflake->setSequenceResolver(function ($currentTime) {
    static $lastTime;
    static $sequence;

    if ($lastTime == $currentTime) {
        ++$sequence;
    } else {
        $sequence = 0;
    }

    $lastTime = $currentTime;

    return $sequence;
})->id();
```

## License

MIT
